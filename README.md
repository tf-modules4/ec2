AWS EC2 Instance Terraform module
=====================================

[![Amol Ovhal][amol_avatar]][amol_ovhal]

[Amol Ovhal][amol_ovhal] 

  [amol_ovhal]: http://www.portfolio.amolovhal.com/
  [amol_avatar]: https://gitlab.com/uploads/-/system/user/avatar/10044525/avatar.png

Terraform module which creates EC2 Instance on AWS.


Terraform versions
------------------

Terraform 0.12.

Usage
------

```hcl
provider "aws" {
  region = "ap-south-1"
}

module "instance" {
  source             = "git::https://gitlab.com/tf-modules4/ec2.git"
  name               = "test"
  instance_type      = "t2.micro"
  key_name           = "key_name"
  volume_size        = 8
  count_ec2_instance = 1
  subnet             = ["subnet-01ca74677a1b41a25"]
  security_groups    = ["sg-018d9b6102b5e9777"]
  ami_id             = "ami-02eb7a4783e7e9317"
  public_ip          = true

}

```

```
output "instance_ip" {
  description = "instance ip"
  value       = module.instance.private_ip
}
output "instance_id" {
  description = "instance id"
  value       = module.instance.instance_id
}
```
Tags
----
* Tags are assigned to resources with name variable as prefix.
* Additial tags can be assigned by tags variables as defined above.

Inputs
------
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| name | The string for name of the instance | `string` | `"false"` | yes |
| instance_type | You can define the type of instance | `string` | `"false"` | yes |
| key_name | Name of the key that you have to define while create an key | `string` | `"false"` | yes |
| volume_size | Define size of EBS volume  | `number` | `"false"` | yes |
| count_ec2_instance | Define count of EC2 instances  | `number` | `"1"` | no |
| subnet |define subnet to launch ec2 instace to particular subnet | `string` | `"false"` | yes |
| security_groups |define security group to attach an instace to particular subnet | `list` | `"false"` | yes |
| ami_id |define ami_id for ec2 instance | `string` | `"false"` | yes |
| public_ip |define public_ip for ec2 instance | `bool` | `"false"` | yes |


Output
------
| Name | Description |
|------|-------------|
| instance_ip | The IP of the instance |
| instance_id | The ID of the instance |

## Related Projects

Check out these related projects.

- [security_group](https://gitlab.com/tf-modules4/security-group) - Terraform module for creating dynamic Security group.

### Contributor :

[![Amol Ovhal][amol_avatar]][amol_homepage]<br/>[Amol Ovhal][amol_homepage] 

  [amol_homepage]: https://gitlab.com/amol.ovhal
  [amol_avatar]: https://gitlab.com/uploads/-/system/user/avatar/10044525/avatar.png
